package edu.usf.cutr.newtrack;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,OnMapReadyCallback {


    private GoogleMap mMap;
    public static final long  UPDATE_INTERVAL_IN_MS = 30000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MS =
            UPDATE_INTERVAL_IN_MS / 4;
    protected LocationRequest mLocationRequest;
    myFirebaseRef = new Firebase("newtrack.edu.usf.cutr");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(mLastLocation.getLatitude(),
                        mLastLocation.getLongitude()),
                MAP_ZOOM_LEVEL));
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        mLastUpdateTime = dateFormat.format(date).toString();
        saveToFirebase();
        // Retrieve saved locations and draw as marker on map
        drawLocations();
        // Update UI to draw bread crumb with the latest bus location.
        mMap.clear();
        LatLng mLatlng = new LatLng(mCurrentLocation.getLatitude(),
                mCurrentLocation.getLongitude());
        MarkerOptions mMarkerOption = new MarkerOptions()
                .position(mLatlng)
                .title(mLastUpdateTime).icon(BitmapDescriptorFactory.fromResource(R.drawable.code_the_road_small));
        Marker mMarker = mMap.addMarker(mMarkerOption);
    }

    private void startLogging() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
        }
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }
    private void saveToFirebase() {
        Map mLocations = new HashMap();
        mLocations.put("timestamp", mLastUpdateTime);
        Map  mCoordinate = new HashMap();
        mCoordinate.put("latitude", mCurrentLocation.getLatitude());
        mCoordinate.put("longitude", mCurrentLocation.getLongitude());
        mLocations.put("location", mCoordinate);
        myFirebaseRef.push().setValue(mLocations);
    }

    private void drawLocations() {
        // Get only latest logged locations - since 'START' button clicked
        Query queryRef =
                myFirebaseRef.orderByChild("timestamp").startAt(startLoggingTime);
        // Add listener for a child added at the data at this location
        queryRef.addChildEventListener(new ChildEventListener() {
            LatLngBounds bounds;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map  data = (Map ) dataSnapshot.getValue();
                String timestamp = (String) data.get("timestamp");
                // Get recorded latitude and longitude
                Map  mCoordinate = (HashMap)data.get("location");
                double latitude = (double) (mCoordinate.get("latitude"));
                double longitude = (double) (mCoordinate.get("longitude"));
                // Create LatLng for each locations
                LatLng mLatlng = new LatLng(latitude, longitude);
                // Make sure the map boundary contains the location
                builder.include(mLatlng);
                bounds = builder.build();
                // Add a marker for each logged location
                MarkerOptions mMarkerOption = new MarkerOptions()
                        .position(mLatlng)
                        .title(timestamp)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.measle_blue));
                Marker mMarker = mMap.addMarker(mMarkerOption);
                markerList.add(mMarker);
                // Zoom map to the boundary that contains every logged location
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                        MAP_ZOOM_LEVEL));
            }
            //Override other abstract methods for addChildEventListener below
        });
    }

}

